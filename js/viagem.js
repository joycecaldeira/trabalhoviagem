var motorista = {};
var veiculos = {};
var funcionarios = {};

url = "http://104.236.122.55:80/doctum/pw/tp1/route.php";

function getAllMotoristas(){
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: 'http://104.236.122.55:80/doctum/pw/tp1/route.php',
		headers: {'table':'motorista'},

	}).success(function( data ){
		motoristas = data;
	}).error(function( error ){
		console.log(error);
	});
}
function getAllCarros(){
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: 'http://104.236.122.55:80/doctum/pw/tp1/route.php',
		headers: {'table':'veiculo'},

	}).success(function( data ){
		veiculos = data;
	}).error(function( error ){
		console.log(error);
	});
}
function getAllFuncionarios(){
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: 'http://104.236.122.55:80/doctum/pw/tp1/route.php',
		headers: {'table':'funcionario'},

	}).success(function( data ){
		funcionarios = data;
	}).error(function( error ){
		console.log(error);
	});
}

function atualizar( id, data, table ) {
	$.ajax({
		type: 'POST',
		contentType: 'json',
		cache: false,
		url:'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id,
		headers: {'table': table},
		data: data

	}).success(function( data ){
		console.log(data);
	}).error(function( error ){
		console.log(error);
	});
}
function editar( id ){
	document.getElementById("form").reset();
	$.ajax({
		type: 'GET',
		dataType: 'json',
		url: 'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id,
		headers: {'table':'viagem'},

	}).success(function( data ){
		var funcs = new Array();
		for(var i=0; i<data[0].funcionarios.length; i++){
			funcs.push(data[0].funcionarios[i].funcionario__id);
		}
		console.log(funcs);
		$('#id').val(data[0].id);
		$("#motorista").val(data[0].motorista__id), 
	    $("#carro").val(data[0].carro__id), 
	    $("#saida").val(data[0].saida), 
	    $("#retorno").val(data[0].retorno),
	    $("#funcionarios").val(funcs)
	}).error(function( error ){
		console.log(error);
	});
}
function excluir( id, table ) {
	var deletar = confirm("Deseja realmente excluir este registro?");
	if(deletar){
	$.ajax({
		type: 'DELETE',
		contentType: 'json',
		cache: false,
		url:'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id, 
		headers: {table: 'viagem'}

	}).success(function( data ){
		console.log(data);
	}).error(function( error ){
		console.log(error);
	});
	}
}
function salvar( data, table ){
	$.ajax({
		type: 'POST',
		contentType: 'json',
		cache: false,
		url:'http://104.236.122.55:80/doctum/pw/tp1/route.php',
		headers: {'table': table},
		data: data

	}).success(function( data ){
		console.log(data);
	}).error(function( error ){
		console.log(error);
	});
}

function atualizarTabelaViagem(){
	document.getElementById("form").reset();

	var tabela = document.getElementById('dados-viagem');
	tabela.innerHTML = "";
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url:'http://104.236.122.55:80/doctum/pw/tp1/route.php',
		headers: {'table':'viagem'},

	}).success(function( viagem ){
		for (var i = 0; i<viagem.length;i++){
			var linha=document.createElement("tr");
			var cid=document.createElement("td");
			var cmotorista=document.createElement("td");
			var ccarro=document.createElement("td");
			var csaida=document.createElement("td");
			var cretorno=document.createElement("td");
			var cbuttons=document.createElement("td");

			var vid = document.createTextNode(viagem[i].id);
			var vmotorista = document.createTextNode(viagem[i].motorista__nome);
			var vcarro = document.createTextNode(viagem[i].carro__modelo);
			var vfuncionarios = document.createTextNode(viagem[i].funcionarios__nome);
			var vsaida = document.createTextNode(viagem[i].saida);
			var vretorno = document.createTextNode(viagem[i].retorno);

			var btnEdit = document.createElement('Button');
			btnEdit.setAttribute('type','button');
			btnEdit.setAttribute('class','btn btn-outline-info');
			btnEdit.setAttribute('id','editar');
			btnEdit.setAttribute('onclick','editar('+viagem[i].id+')');
			btnEdit.setAttribute('data-toggle','modal');
			btnEdit.setAttribute('data-target','#exampleModal');
			btnEdit.title = 'Editar';
			btnEdit.innerHTML = '<i class="fa fa-edit"></i>';

			var btnEx = document.createElement('Button');
			btnEx.setAttribute('type','button');
			btnEx.setAttribute('class','btn btn-outline-danger');
			btnEx.setAttribute('id','excluir');
			btnEx.setAttribute('onclick','excluir('+viagem[i].id+')');
			btnEx.title = 'Excluir';
			btnEx.innerHTML = '<i class="fa fa-trash-alt"></i>';

			cid.appendChild(vid);
			cmotorista.appendChild(vmotorista);
			ccarro.appendChild(vcarro);
			csaida.appendChild(vsaida);
			cretorno.appendChild(vretorno);
			cbuttons.appendChild(btnEdit);
			cbuttons.appendChild(btnEx);

			linha.appendChild(cid);
			linha.appendChild(cmotorista);
			linha.appendChild(ccarro);
			linha.appendChild(csaida);
			linha.appendChild(cretorno);
			linha.appendChild(cbuttons);
			tabela.appendChild(linha);
		}
	}).error(function( error ){
		console.log(error);
	});
	
	
}

function Selects(){
	$('#motorista').empty();
	$('#carro').empty();
	$('#funcionarios').empty();
	for(var i = 0; i<motoristas.length;i++){
		var option = document.createElement('option');
		$(option).attr({value: motoristas[i].id});
		$(option).append( motoristas[i].nome );

		$('#motorista').append(option);
	}
	for(var i = 0; i<veiculos.length;i++){
		var option = document.createElement('option');
		$(option).attr({value: veiculos[i].id});
		$(option).append( veiculos[i].modelo );

		$('#carro').append(option);
	}

	for(var i = 0; i<funcionarios.length;i++){
		var option = document.createElement('option');
		$(option).attr({value: funcionarios[i].id});
		$(option).append( funcionarios[i].nome );

		$('#funcionarios').append(option);
	}

	console.log('ok');
}


$("#atualizar").on('click',function(){
	atualizarTabelaViagem();
});

$("#salvar").on('click',function(){
	if( $("#id").val() != "" ) {
		var data = JSON.stringify({ 
			id: $("#id").val(),
		    motorista: parseInt( $("#motorista").val() ), 
		    carro: parseInt( $("#carro").val() ), 
		    saida: $("#saida").val(), 
		    retorno: $("#retorno").val(),
		    funcionarios: $("#funcionarios").val()
	    });
	    console.log(data);
		atualizar( data.id, data, "viagem" );
	} else {
		var data = JSON.stringify({ 
		    motorista: $("#motorista").val(), 
		    carro: $("#carro").val(), 
		    saida: $("#saida").val(), 
		    retorno: $("#retorno").val(),
		    funcionarios: $("#funcionarios").val()
	    });
	    console.log(data);
		salvar(data, "viagem");
		atualizarTabelaViagem();
	}
});

$(document).ready(function(){
	getAllMotoristas();
	getAllCarros();
	getAllFuncionarios();

	setTimeout(Selects,2000);
	atualizarTabelaViagem();
});