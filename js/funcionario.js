url = "http://104.236.122.55:80/doctum/pw/tp1/route.php";

var funcionarios = {};

function atualizar( id, data, table ) {
	$.ajax({
		type: 'POST',
		contentType: 'json',
		cache: false,
		url:'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id,
		headers: {'table': table},
		data: data

	}).success(function( data ){
		console.log(data);
	}).error(function( error ){
		console.log(error);
	});
}


function salvar( data, table ){
	$.ajax({
		type: 'POST',
		contentType: 'json',
		cache: false,
		url:'http://104.236.122.55:80/doctum/pw/tp1/route.php',
		headers: {'table': table},
		data: data

	}).success(function( data ){
		console.log(data);
	}).error(function( error ){
		console.log(error);
	});
}
function excluir( id, table ) {
	var deletar = confirm("Deseja realmente excluir este registro?");
	if(deletar){
	$.ajax({
		type: 'DELETE',
		contentType: 'json',
		cache: false,
		url:'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id, 
		headers: {table: 'funcionario'}

	}).success(function( data ){
		console.log(data);
	}).error(function( error ){
		console.log(error);
	});
	}
}
function editar( id ) {
	document.getElementById("form").reset();
	$.ajax({
		type: 'GET',
		dataType: 'json',
		url: 'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id,
		headers: {'table':'funcionario'},

	}).success(function( data ){
		console.log( data);
		$('#id').val(data[0].id);
		$('#nome').val(data[0].nome);
		$('#funcao').val(data[0].funcao);
		$('#telefoneFixo').val(data[0].telefone_fixo);
		$('#telefoneMovel').val(data[0].telefone_movel);
		$('#email').val(data[0].email);
	}).error(function( error ){
		console.log(error);
	});
}

	$("#salvar").on('click',function(){
		if( $("#id").val() != "" ) {
			var data = JSON.stringify({ 
				id: $("#id").val(),
			    nome: $("#nome").val(), 
			    funcao: $("#funcao").val(), 
			    email: $("#email").val(), 
			    telefone_movel: $("#telefoneMovel").val(), 
			    telefone_fixo: $("#telefoneFixo").val()
		    });
			atualizarTabelaFuncionarios();
			console.log(data);
		    atualizar(data.id, data, "funcionario");
		}else{
			var data = JSON.stringify({ 
			    nome: $("#nome").val(), 
			    funcao: $("#funcao").val(), 
			    email: $("#email").val(), 
			    telefone_movel: $("#telefoneMovel").val(), 
			    telefone_fixo: $("#telefoneFixo").val()
		    });
			console.log(data);
			atualizarTabelaFuncionarios();
			salvar(data, "funcionario");
		}
		
	});


function atualizarTabelaFuncionarios(){
	document.getElementById("form").reset();
	var tabela = document.getElementById('dados-funcionarios');
	tabela.innerHTML = "";
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: 'http://104.236.122.55:80/doctum/pw/tp1/route.php',
		headers: {'table':'funcionario'},
	}).success(function( funcionarios){
		for (var i = 0; i<funcionarios.length;i++){
			if( funcionarios[i].email != null ){
				var linha=document.createElement("tr");
				var cid=document.createElement("td");
				var cnome=document.createElement("td");
				var cfuncao=document.createElement("td");
				var ctelefoneFixo=document.createElement("td");
				var ctelefoneMovel=document.createElement("td");
				var cemail=document.createElement("td");
				var cbuttons=document.createElement("td");


				var vid= document.createTextNode(funcionarios[i].id);
				var vnome= document.createTextNode(funcionarios[i].nome);
				var vfuncao= document.createTextNode(funcionarios[i].funcao);
				var vtelefoneFixo= document.createTextNode(funcionarios[i].telefone_fixo);		
				var vtelefoneMovel= document.createTextNode(funcionarios[i].telefone_movel);
				var vemail=document.createTextNode(funcionarios[i].email);	


				var btnEdit = document.createElement('Button');
				btnEdit.setAttribute('type','button');
				btnEdit.setAttribute('class','btn btn-outline-info');
				btnEdit.setAttribute('id','editar');
				btnEdit.setAttribute('onclick','editar(value)');
				btnEdit.setAttribute('data-toggle','modal');
				btnEdit.setAttribute('data-target','#exampleModal');
				btnEdit.setAttribute('value',funcionarios[i].id);
				btnEdit.title = 'Editar';
				btnEdit.innerHTML = '<i class="fa fa-edit"></i>';

				var btnEx = document.createElement('Button');
				btnEx.setAttribute('type','button');
				btnEx.setAttribute('class','btn btn-outline-danger');
				btnEx.setAttribute('id','excluir');
				btnEx.setAttribute('onclick','excluir(value)');
				btnEx.setAttribute('value',funcionarios[i].id);
				btnEx.title = 'Excluir';
				btnEx.innerHTML = '<i class="fa fa-trash-alt"></i>';

				cid.appendChild(vid);
				cnome.appendChild(vnome);
				cfuncao.appendChild(vfuncao);
				ctelefoneFixo.appendChild(vtelefoneFixo);
				ctelefoneMovel.appendChild(vtelefoneMovel);
				cemail.appendChild(vemail);
				cbuttons.appendChild(btnEdit);
				cbuttons.appendChild(btnEx);

				linha.appendChild(cid);
				linha.appendChild(cnome);
				linha.appendChild(cfuncao);
				linha.appendChild(ctelefoneFixo);
				linha.appendChild(ctelefoneMovel);
				linha.appendChild(cemail);
				linha.appendChild(cbuttons);
				tabela.appendChild(linha);
			}
		}
	}).error(function( error ){
		console.log(error);
	});
}
$("#atualizar").on('click',function(){
	atualizarTabelaFuncionarios();
});


$(document).ready(function(){
	atualizarTabelaFuncionarios();
});

$('.cad').on('click',function(){
	document.getElementById("form").reset();
});