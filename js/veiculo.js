url = "http://104.236.122.55:80/doctum/pw/tp1/route.php";

 var veiculo = {};

 function atualizar( id, data, table ) {
	$.ajax({
		type: 'POST',
		contentType: 'json',
		cache: false,
		url:'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id,
		headers: {'table': table},
		data: data

	}).success(function( data ){
		console.log(data);
	}).error(function( error ){
		console.log(error);
	});
}

function salvar( data, table ){
	$.ajax({
		type: 'POST',
		contentType: 'json',
		cache: false,
		url:'http://104.236.122.55:80/doctum/pw/tp1/route.php',
		headers: {'table': table},
		data: data

	}).success(function( data ){
		console.log(data);
	}).error(function( error ){
		console.log(error);
	});
}
function excluir( id, table ) {
	var deletar = confirm("Deseja realmente excluir este registro?");
	if(deletar){
	$.ajax({
		type: 'DELETE',
		contentType: 'json',
		cache: false,
		url:'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id, 
		headers: {table: 'veiculo'}

	}).success(function( data ){
		console.log(data);
	}).error(function( error ){
		console.log(error);
	});
	}
}

function editar( id ){
	document.getElementById("form").reset();
	$.ajax({
		type: 'GET',
		dataType: 'json',
		url: 'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id,
		headers: {table:'veiculo'},

	}).success(function( data ){
		console.log( data);
		$('#id').val(data[0].id);
		$('#modelo').val(data[0].modelo);
		$('#qtde_passageiros').val (data[0].qtde_passageiros);
		$('#ativo').prop("checked",parseInt( data[0].ativo));
	}).error(function( error ){
		console.log(error);
	});

}


$("#salvar").on('click',function(){
		if( $("#id").val() != "" ) {
			var data = JSON.stringify({ 
				id: $("#id").val(),
			    modelo: $("#modelo").val(), 
			    qtde_passageiros: $("#qtde_passageiros").val(), 
			    ativo: document.getElementById('ativo').checked 
			    
		    });
			atualizarTabela();
			console.log(data);
		    atualizar(data.id, data, "veiculo");
		}else{
			var data = JSON.stringify({ 
			    modelo: $("#modelo").val(), 
			    qtde_passageiros: $("#qtde_passageiros").val(), 
			    ativo: document.getElementById('ativo').checked 
		    });
		    console.log(data);
			salvar(data, "veiculo");
			atualizarTabela();
		}
		
	});


function atualizarTabela(){
	document.getElementById("form").reset();
	var tabela = document.getElementById('dados-veiculo');
	tabela.innerHTML = "";
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: 'http://104.236.122.55:80/doctum/pw/tp1/route.php',
		headers: {table:'veiculo'}

	}).success(function( veiculos){
		console.log(veiculos);
		for (var i = 0; i<veiculos.length;i++){
			if( veiculos[i].modelo != null ){
				var linha=document.createElement("tr");
				var cid=document.createElement("td");
				var cmodelo=document.createElement("td");
				var cqtde_passageiros=document.createElement("td");
				var cativo=document.createElement("td");
				var cbuttons=document.createElement("td");


				var vid= document.createTextNode(veiculos[i].id);
				var vmodelo= document.createTextNode(veiculos[i].modelo);
				var vqtde_passageiros= document.createTextNode(veiculos[i].qtde_passageiros);
				var vativo=document.createTextNode(veiculos[i].ativo);	


				var btnEdit = document.createElement('Button');
				btnEdit.setAttribute('type','button');
				btnEdit.setAttribute('class','btn btn-outline-info');
				btnEdit.setAttribute('id','editar');
				btnEdit.setAttribute('onclick','editar(value)');
				btnEdit.setAttribute('data-toggle','modal');
				btnEdit.setAttribute('data-target','#exampleModal');
				btnEdit.setAttribute('value',veiculos[i].id);
				btnEdit.title = 'Editar';
				btnEdit.innerHTML = '<i class="fa fa-edit"></i>';

				var btnEx = document.createElement('Button');
				btnEx.setAttribute('type','button');
				btnEx.setAttribute('class','btn btn-outline-danger');
				btnEx.setAttribute('id','excluir');
				btnEx.setAttribute('onclick','excluir(value)');
				btnEx.setAttribute('value',veiculos[i].id);
				btnEx.title = 'Excluir';
				btnEx.innerHTML = '<i class="fa fa-trash-alt"></i>';

				cid.appendChild(vid);
				cmodelo.appendChild(vmodelo);
				cqtde_passageiros.appendChild(vqtde_passageiros);
				cativo.appendChild(vativo);
				cbuttons.appendChild(btnEdit);
				cbuttons.appendChild(btnEx);

				linha.appendChild(cid);
				linha.appendChild(cmodelo);
				linha.appendChild(cqtde_passageiros);
				linha.appendChild(cativo);
				linha.appendChild(cbuttons);
				tabela.appendChild(linha);
			}
		}
	}).error(function( error ){
		console.log(error);
	});
}
$("#atualizar").on('click',function(){
	atualizarTabela();
});


$(document).ready(function(){
	atualizarTabela();
});
$('.cad').on('click',function(){
	document.getElementById("form").reset();
});
